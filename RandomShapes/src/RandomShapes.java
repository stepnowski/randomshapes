import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.util.Random;

public class RandomShapes extends JPanel
{
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g); 
		Random randomNumber = new Random();
		
		int width = getWidth();
		int height = getHeight();
		
		for (int i = 1; i<=10;i++)
		{
			int shape = randomNumber.nextInt(2);//0 is rect, 1 is oval
			
			if (shape==0)//draw rectangle
			{
				Color colorRect = new Color(randomNumber.nextInt(256),randomNumber.nextInt(256),randomNumber.nextInt(256));
				g.setColor(colorRect);
				g.fillRect(randomNumber.nextInt(width),randomNumber.nextInt(height),randomNumber.nextInt(width)/2,randomNumber.nextInt(height)/2);
			}
			else//draw oval
			{
				Color colorOval = new Color(randomNumber.nextInt(256),randomNumber.nextInt(256),randomNumber.nextInt(256));
				g.setColor(colorOval);
				g.fillOval(randomNumber.nextInt(width),randomNumber.nextInt(height),randomNumber.nextInt(width)/2,randomNumber.nextInt(height)/2);
			}
			
		}
		
		
		
	}
}
